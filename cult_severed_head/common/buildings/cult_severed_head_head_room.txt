castle = {
	ca_head_room_1 = {
		desc = ca_head_room_1_desc
		potential = {
			OR = {
				FROMFROM = { has_building = ca_head_room_1 }
			}
		}
		# monthly_character_prestige = 0.5
        liege_prestige = 0.25
		# convert_to_tribal = tb_head_room_1
	}
	ca_head_room_2 = {
		desc = ca_head_room_2_desc
		potential = {
			OR = {
				FROMFROM = { has_building = ca_head_room_2 }
			}
		}
		upgrades_from = ca_head_room_1
        land_morale = 0.2
        levy_reinforce_rate = 0.2
		# convert_to_tribal = tb_head_room_2
	}
	ca_head_room_3 = {
		desc = ca_head_room_3_desc
		potential = {
			OR = {
				FROMFROM = { has_building = ca_head_room_3 }
			}
		}
		upgrades_from = ca_head_room_2
        tech_growth_modifier_military = 0.05
        military_techpoints = 0.05
		# convert_to_tribal = tb_head_room_3
	}
	ca_head_room_4 = {
		desc = ca_head_room_4_desc
		potential = {
			OR = {
				FROMFROM = { has_building = ca_head_room_4 }
			}
		}
		upgrades_from = ca_head_room_3
		levy_size = 0.05
		# convert_to_tribal = tb_head_room_4
	}
}
tribal = {
	tb_head_room_1 = {
		desc = ca_head_room_1_desc
		potential = {
			OR = {
				FROMFROM = { has_building = tb_head_room_1 }
			}
		}
		# monthly_character_prestige = 0.5
        liege_prestige = 0.25
		convert_to_castle = ca_head_room_1
	}
	tb_head_room_2 = {
		desc = ca_head_room_2_desc
		potential = {
			OR = {
				FROMFROM = { has_building = tb_head_room_2 }
			}
		}
		upgrades_from = tb_head_room_1
        land_morale = 0.2
        levy_reinforce_rate = 0.2
		convert_to_castle = ca_head_room_2
	}
	tb_head_room_3 = {
		desc = ca_head_room_3_desc
		potential = {
			OR = {
				FROMFROM = { has_building = tb_head_room_3 }
			}
		}
		upgrades_from = tb_head_room_2
        tech_growth_modifier_military = 0.05
        military_techpoints = 0.05
		convert_to_castle = ca_head_room_3
	}
	tb_head_room_4 = {
		desc = ca_head_room_4_desc
		potential = {
			OR = {
				FROMFROM = { has_building = tb_head_room_4 }
			}
		}
		upgrades_from = tb_head_room_3
		levy_size = 0.05
		convert_to_castle = ca_head_room_4
	}
}

# TODO - add city version! (ct)
