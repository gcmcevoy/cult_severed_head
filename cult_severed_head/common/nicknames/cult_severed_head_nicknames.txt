#########################
## TRIGGERED NICKNAMES ##
#########################

nick_of_heads = {
	allow = {
        society_member_of = cult_severed_head
		OR = {
            has_artifact = severed_head_enemy
            has_artifact = severed_head_enemy_2
            has_artifact = severed_head_enemy_3
            has_artifact = severed_head_legend
            has_artifact = severed_head_pope
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			trait = head1
		}
		modifier = {
			factor = 1.5
			trait = head2
		}
		modifier = {
			factor = 1.5
			trait = lunatic
		}
		modifier = {
			factor = 1.5
			trait = wroth
		}
		modifier = {
			factor = 1.5
			martial = 30
		}
		modifier = {
			factor = 2
			society_rank == 4
		}
	}
}
